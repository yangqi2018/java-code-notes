package com.yang;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.*;
import com.deepoove.poi.data.style.BorderStyle;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import cn.hutool.core.bean.BeanUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

@DisplayName("测试doc文档动态动态导出内容")
public class DocTest {
    String resource = "src/test/resources/template/test.docx";


    @Test
    public void testPaymentExample() throws Exception {
        DocData docData = new DocData();
        docData.setOrgName("川西北气矿");
        List<TitleData> datas = new ArrayList<>();

        TitleData data = new TitleData();
        data.setTitle("测试标题");
        data.setText("1）气田开发管理部、管道管理部：负责组织分公司完整性管理体系的建立、完整性管理实施规划、年度工作方案和总结的编制；协调和监督各二级单位按照分公司的总体要求实施完整性管理；组织分公司完整性管理科研工作，包括科研规划、项目申请和技术攻关等；负责组织完整性管理相关培训的实施工作；负责分控项目的立项、审批和验收；组织分公司完整性管理的监督、检查与考核。\n" +
                "   2）规划计划处：负责建设前期的完整性管理、完整性管理项目投资的落实和平衡。\n" +
                "   3）质量安全环保处：负责完整性管理所涉及的QHSE相关工作的监督指导。\n" +
                "   4）生产运行处：负责完整性管理中的地质灾害防治。\n" +
                "   5）基建工程处：负责建设施工期的完整性管理。");
        RowRenderData header = Rows.of("日期", "订单编号", "销售代表", "离岸价", "发货方式", "条款", "税号").bgColor("F2F2F2").center()
                .textColor("7F7f7F").textFontFamily("Hei").textFontSize(9).create();
        RowRenderData row = Rows.of("2018-06-12", "SN18090", "李四", "5000元", "快递", "附录A", "T11090").center().create();
        BorderStyle borderStyle = new BorderStyle();
        borderStyle.setColor("A6A6A6");
        borderStyle.setSize(4);
        borderStyle.setType(XWPFTable.XWPFBorderType.SINGLE);
        TableRenderData table = Tables.ofA4MediumWidth().addRow(header).addRow(row).border(borderStyle).center()
                .create();
        data.setTable(table);

        for (int i = 0; i < 3; i++) {
            TitleData data1 = new TitleData();
            List<TitleData> datas1 = new ArrayList<>();
            for (int j = 0; j < 2; j++) {
                TitleData data2 = new TitleData();
                BeanUtil.copyProperties(data,data2);
                data2.setTitle("测试标题"+j+j);
                datas1.add(data2);
            }
            DocxRenderData segment1 = new DocxRenderData(new File("src/test/resources/template/title2.docx"), datas1);
            BeanUtil.copyProperties(data,data1);
            data1.setSegment(segment1);
            data1.setTitle("测试标题"+i);
            datas.add(data1);
        }

        datas.add(data);


        DocxRenderData segment = new DocxRenderData(new File("src/test/resources/template/title1.docx"), datas);
        docData.setSegment(segment);

        XWPFTemplate template = XWPFTemplate.compile("src/test/resources/template/test.docx").render(docData);

        FileOutputStream out = new FileOutputStream("target/out.docx");
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }
}
