package com.yang;

import com.deepoove.poi.data.DocxRenderData;

public class DocData {
    private String orgName;
    private DocxRenderData segment;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public DocxRenderData getSegment() {
        return segment;
    }

    public void setSegment(DocxRenderData segment) {
        this.segment = segment;
    }
}
