package com.yang;

import com.deepoove.poi.data.DocxRenderData;
import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.data.TableRenderData;

import java.util.List;

public class TitleData {

    private String title;
    private String text;
    private PictureRenderData pic;
    private List<TitleData> children;
    private TableRenderData table;
    private DocxRenderData segment;

    public DocxRenderData getSegment() {
        return segment;
    }

    public void setSegment(DocxRenderData segment) {
        this.segment = segment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public PictureRenderData getPic() {
        return pic;
    }

    public void setPic(PictureRenderData pic) {
        this.pic = pic;
    }

    public List<TitleData> getChildren() {
        return children;
    }

    public void setChildren(List<TitleData> children) {
        this.children = children;
    }

    public TableRenderData getTable() {
        return table;
    }

    public void setTable(TableRenderData table) {
        this.table = table;
    }
}
